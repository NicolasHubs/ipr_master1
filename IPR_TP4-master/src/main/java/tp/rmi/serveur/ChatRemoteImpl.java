package tp.rmi.serveur;

import tp.rmi.common.ChatRemote;
import tp.rmi.common.ReceiveCallback;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class ChatRemoteImpl extends UnicastRemoteObject implements ChatRemote, ReceiveCallback {

    private List<ReceiveCallback> myCallbacks = new ArrayList<>();

    public ChatRemoteImpl() throws RemoteException {
        super();
    }

    @Override
    public String echo(String name, String message) throws RemoteException {
        return name + ">" + message;
    }

    @Override
    public void send(String name, String message) throws RemoteException {
        for (ReceiveCallback callback : myCallbacks)
            callback.newMessage(name + ">" + message);
    }

    @Override
    public void registerCallback(ReceiveCallback callback) throws RemoteException {
        myCallbacks.add(callback);
    }

    @Override
    public void unregisterCallback(ReceiveCallback callback) throws RemoteException {
        myCallbacks.remove(callback);
    }

    @Override
    public void newMessage(String message) throws RemoteException {
        System.out.println(message);
    }
}
