package tp.rmi.serveur;

import tp.rmi.common.ChatRemote;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.file.Paths;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ChatServeur {

    public static void main(String[] args) throws IOException {
        try { // On utilise un try pour gérer les erreurs proprement :
            String pathToClasses = Paths.get("target/classes/").toUri().toURL().toString();
            System.setProperty("java.rmi.server.codebase", pathToClasses);

            //On donne la bonne ip :
            System.setProperty("java.rmi.server.hostname", InetAddress.getLocalHost().getHostAddress());

            // Url d'enregistrement de l'objet :
            String url = "rmi://" + InetAddress.getLocalHost().getHostAddress() + "/chat";

            // La création de l'objet et l'enregistrement de l'url dans le registre
            ChatRemote objetDistant = new ChatRemoteImpl();

            Registry registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
            registry.rebind(url, objetDistant);

            // Naming.rebind("//" + getLocalIP().getHostAddress() + "/chat", objetDistant);

            System.out.println(objetDistant + " has been registered");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static InetAddress getLocalIP() throws IOException {
        Socket s = new Socket("google.fr", 80);
        InetAddress result = s.getLocalAddress();
        s.close();
        return result;
    }

}
