package tp.rmi.common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChatRemote extends Remote {

    String echo(String name, String message) throws RemoteException;

    void send(String name, String message) throws RemoteException;

    void registerCallback(ReceiveCallback callback) throws RemoteException;

    void unregisterCallback(ReceiveCallback callback) throws RemoteException;

}