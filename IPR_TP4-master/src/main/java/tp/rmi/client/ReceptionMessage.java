package tp.rmi.client;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * @author Nicolas CROQ et Alexis ESNAULT groupe 1.1 <nicolas.croq@etudiant.univ-rennes1.fr>
 * @version 1.0
 * @since 2018-02-06
 */
public class ReceptionMessage implements Runnable {

	private BufferedReader in;
	private boolean lecture = true;

	public ReceptionMessage(BufferedReader in) {
		this.in = in;
	}

	public void arret() {
		lecture = false;
	}

	@Override
	public void run() {
		while (lecture) {
			try {
				System.out.println(ClientTCP.recevoirMessage(in));
			} catch (IOException e) {
				arret();
				System.err.println("Le serveur est probablement ferm�, arret de la reception des messages.");
			}
		}
	}
}
