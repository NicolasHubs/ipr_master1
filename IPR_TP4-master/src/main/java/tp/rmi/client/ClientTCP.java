package tp.rmi.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @author Nicolas CROQ et Alexis ESNAULT groupe 1.1 <nicolas.croq@etudiant.univ-rennes1.fr>
 * @version 1.0
 * @since 2018-02-06
 */
public class ClientTCP {
	private static String charset = String.valueOf(StandardCharsets.UTF_8);
	
	public static void main(String[] args) {
		int port = 8080;
		String host = "localhost";
		String message, username = "";
		ReceptionMessage reception = null;
		Socket socketVersLeServeur = null;
		
		// cr�er une socket client
		try {
			socketVersLeServeur = new Socket(host, port);
			if (args.length >= 2) charset = String.valueOf(args[1]);

			// cr�er reader et writer associ�s
			PrintWriter out = creerPrinter(socketVersLeServeur, charset);
			BufferedReader in = creerReader(socketVersLeServeur, charset);
			
			reception = new ReceptionMessage(in);
			Thread t = new Thread (reception);
			t.start();
			
			if (args.length != 0) username = args[0];
			envoyerNom(out, username);
			
			// Tant que le mot �fin� n�est pas lu sur le clavier
			// Lire un message au clavier
			while (!("fin").equals((message = lireMessageAuClavier())))
				envoyerMessage(out, message); // envoyer le message au serveur
			
			reception.arret();
		} catch (IOException e) {
			reception.arret();
			System.err.println("Le serveur n'est pas disponible");
		} finally {
			reception.arret();
			try {
				socketVersLeServeur.close();
			} catch (IOException e) {
				System.err.println("La socket n'existe plus.");
			}
		}
	}

	public static String lireMessageAuClavier() throws IOException {
		// lit un message au clavier en utilisant par exemple un BufferedReader sur System.in
		//System.out.println("Saisissez un message :");
		return new BufferedReader(new InputStreamReader(System.in, charset)).readLine();
	}

	public static BufferedReader creerReader(Socket socketVersLeServeur, String charset) throws IOException {
		// cr�er un reader qui utilise le bon charset
		return new BufferedReader(new InputStreamReader(socketVersLeServeur.getInputStream(), charset));
	}

	public static PrintWriter creerPrinter(Socket socketVersLeServeur, String charset) throws IOException {
		// cr�er un printer qui utilise le bon charset
		return new PrintWriter(new OutputStreamWriter(socketVersLeServeur.getOutputStream(), charset));
	}

	public static String recevoirMessage(BufferedReader reader) throws IOException {
		// R�cup�rer une ligne
		// Retourner la ligne lue ou null si aucune ligne � lire.
		return reader.readLine();
	}

	public static void envoyerMessage(PrintWriter printer, String message) throws IOException {
		// Envoyer le message vers le serveur
		printer.println(message);
		printer.flush();
	}

	public static void envoyerNom(PrintWriter printer, String nom) throws IOException {
		// envoi � NAME: nom � au serveur
		envoyerMessage(printer, "NAME: " + nom);
	}

}