package tp.rmi.client;

import tp.rmi.common.ReceiveCallback;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Afficheur extends UnicastRemoteObject implements ReceiveCallback {

    public Afficheur() throws RemoteException {
        super();
    }

    @Override
    public void newMessage(String message) throws RemoteException {
        System.out.println(message);
    }
}
