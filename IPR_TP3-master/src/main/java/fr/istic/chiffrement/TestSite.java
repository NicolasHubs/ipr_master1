package fr.istic.chiffrement;

import javax.net.ssl.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.cert.Certificate;

public class TestSite {

    public static void main(String[] args) throws IOException {
        connecteEtAffiche("www.google.fr", 443);
    }

    public static void connecteEtAffiche(String host, int port) throws IOException {
        Charset charset = StandardCharsets.UTF_8;

        // Creation de la socket
        SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(host, port);
/*
        for (String val : sslsocket.getSupportedCipherSuites())
            System.out.println(val);
*/
        // Choix de la cipher suite si nécessaire (sinon laisser defaut).
        // Connexion
        String[] cipherSuite = {
                "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384",
                "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
                "TLS_RSA_WITH_AES_256_GCM_SHA384",
                "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384",
                "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384",
                "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"};

        /*
    SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA,
    SSL_RSA_WITH_3DES_EDE_CBC_SHA,
    TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA,
    TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
    TLS_RSA_WITH_AES_128_GCM_SHA256,
    TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256
*/


        String[] protocols = {"TLSv1.2"};

        // enable TLSv1.2 only
        sslsocket.setEnabledProtocols(protocols);

        // enable a exportable stream cipher
        sslsocket.setEnabledCipherSuites(cipherSuite);

        sslsocket.setUseClientMode(true);

        SSLSession session = sslsocket.getSession();

        if (session.getCipherSuite().equals("SSL_NULL_WITH_NULL_NULL")) {
            throw new IOException("SSL handshake failed. Ciper suite in SSL Session is SSL_NULL_WITH_NULL_NULL");
        }

        // Affichage des infos demandées :
        System.out.println("Session creation : " + session.getCreationTime());
        System.out.println("Protocol : " + session.getProtocol());
        System.out.println("CipherSuite : " + session.getCipherSuite());
        System.out.println("identité serveur : " + session.getPeerPrincipal());
        System.out.println("---");

        System.out.println("Certificats serveur : ");
        for (Certificate val : session.getPeerCertificates())
            System.out.println(val.toString());

        System.out.println("Certificats client : ");
        for (Certificate val : session.getPeerCertificates())
            System.out.println(val.toString());

        // Fermeture de la socket
        sslsocket.close();
    }
}
