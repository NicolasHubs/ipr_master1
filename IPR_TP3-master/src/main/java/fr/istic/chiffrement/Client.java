package fr.istic.chiffrement;

import javax.net.ssl.*;
import java.io.*;
import java.security.KeyStore;
import java.util.Arrays;

public class Client {

    private static char[] PASSWORD = "654321".toCharArray();

    public static void main(String[] args) throws Exception {
        // Création d'un SSLContext comme pour le serveur
        // avec les caractéristiques suivantes :
        // + protocole utilisé : TLS
        SSLContext context = SSLContext.getInstance("TLS");
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        // + format de clef PKCS12
        KeyStore ks = KeyStore.getInstance("PKCS12");

        // ks.load(new FileInputStream("clientstore.keys"), null);
        ks.load(new FileInputStream("clientstore.keys"), PASSWORD);
        // + mot de passe store et clef : variable PASSWORD plus haut (ou null)
        // + keystore : clientstore.keys
        kmf.init(ks, PASSWORD);

        context.init(kmf.getKeyManagers(), null, null);
        Arrays.fill(PASSWORD, '0');

        SSLSocketFactory factory = context.getSocketFactory();
        SSLSocket socket = (SSLSocket) factory.createSocket("localhost", 9999);
        // Creation des printwriter et buffered reader puis :
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

        int nbEnvois = 0;
        String line = null;
        out.write("PING");
        out.flush();
        while ((line = in.readLine()) != null || nbEnvois == 10) {
            System.out.println(in.readLine());
            System.out.println(line);
            out.write("PING");
            out.flush();
            nbEnvois++;
        }
    }
}