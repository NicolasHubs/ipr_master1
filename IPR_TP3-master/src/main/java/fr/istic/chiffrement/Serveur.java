package fr.istic.chiffrement;

import javax.net.ssl.*;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.security.KeyStore;
import java.util.Arrays;

public class Serveur {
    private static int PORT = 9999;
    // NE FAITES PAS CA EN PROD :
    private static char[] PASSWORD = "123456".toCharArray();

    public static void main(String[] args) throws Exception {
        long time = System.currentTimeMillis();
        // Choisir l'un ou l'autre en fonction de la question :
        ServerSocket server = creerSocketTLS(); // creerSocketTLS(); // creerSocketClassique();
        System.out.printf("temps écoulé : %d ms %n", System.currentTimeMillis() - time);
        readPingSendPong(server);
    }

    public static ServerSocket creerSocketClassique() throws Exception {
        // TODO : retourner une socket classique qui écoute sur le port 9999
        return new ServerSocket(PORT);
    }

    /**
     * Créer une ServerSocket TLS qui écoute sur le port 9999
     *
     * @return une socket classique
     * @throws Exception
     */
    public static ServerSocket creerSocketTLS() throws Exception {
        // Créer une ServerSocket TLS qui écoute sur le port 9999, avec les
        // caractéristiques suivantes :
        // + protocole utilisé : TLS
        SSLContext context = SSLContext.getInstance("TLS");
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        // + format de clef PKCS12
        KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(new FileInputStream("serveurstore.keys"), PASSWORD);
        // + mot de passe store et clef : variable PASSWORD plus haut.
        // + keystore, serveurstore.keys
        kmf.init(ks, PASSWORD);

        context.init(kmf.getKeyManagers(), null, null);
        Arrays.fill(PASSWORD, '0');

        SSLServerSocketFactory factory = context.getServerSocketFactory();
        return factory.createServerSocket(PORT); // Retourner la socket correctement configurée
    }

    public static void readPingSendPong(ServerSocket server) throws Exception {
        // accepter une connexion
        // lire PING
        // écrire PONG vers le client
        // openssl s_client -connect localhost:9999

        int bufferSize = 1024;

        ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
        byte[] message;
        boolean isDisconnected = false;
        int nbClient = 0;
        while (true) {
            SSLSocket sslsocket = (SSLSocket) server.accept();
            sslsocket.setEnabledCipherSuites(new String[]{"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"});
            // sslsocket.bind(new InetSocketAddress("localhost", PORT)); // marche po
            System.out.println("Client n°" + ++nbClient);
            InputStream in = sslsocket.getInputStream();
            OutputStream out = sslsocket.getOutputStream();

            while (!isDisconnected) {
                try {
                    while (byteBuffer.hasRemaining()) {
                        int b = in.read();
                        if (b == -1) {
                            isDisconnected = true;
                            break;
                        }

                        byteBuffer.put((byte) b);
                        if (in.available() == 0)
                            break;
                    }
                } catch (SSLException e) {
                    System.err.println("Ce n'est pas un client sécurisé.");
                    break;
                }
                byteBuffer.flip();

                if (isDisconnected)
                    break;

                message = new byte[byteBuffer.limit()];

                for (int i = 0; i < message.length; i++) {
                    message[i] = byteBuffer.get(i);
                }
                String result = new String(message).toUpperCase().replaceAll("\n", "");
                if ("PING".equals(result))
                    out.write("PONG\n".getBytes());

                byteBuffer.clear();
            }
            in.close();
            out.close();
            sslsocket.close();
        }
    }
}