# TP 7 : Les services REST

## Resource Hello
**Test de la ressource sur** http://localhost:8080/tp7_ipr/api/hello/toto

![](pictures/test_ressource.png "pictures/test_ressource.png")

## Interroger la ressource depuis une servlet
**Test de la ressource Hello sur** http://localhost:8080/tp7_ipr/client

![](pictures/test_client_ressource.png "pictures/test_client_ressource.png")

## Ecriture d'un serveur chat

**Exposition de l'API REST :**

URL | Type de requête | Description
:- | :- | :-
/messages | GET | avoir la liste des messages
/messages/after/{id} | GET | ajouter un message. Le message envoyé n'a pas d'id ni de date. C'est le serveur qui les initialise.
/messages | POST | ajouter un message. Le message envoyé n'a pas d'id ni de date. C'est le serveur qui les initialise.
/messages/{id} | GET | avoir le message d'ID {id}
/messages/{id} | DELETE | effacer le message d'ID {id}

### Questions

**1. Faites en sorte que la classe Message puisse être passée en XML.**

Il suffit d'ajouter cette annotation au dessus de la méthode concernée (soit la méthode de POST qui "consume" les messages):
```java 
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
```

**2. Ecrire une classe `MessageResource.java` dans le package que vous avez configurer pour accueillir les ressources REST (vous trouverez le nom du package dans le fichier web.xml). Cette classe permet de réaliser les actions souhaitées.**

**3. Ecrire les méthodes de l'API (JSON + XML, codes de statut et location).**

*Réponse à la question 2 et 3 :*

```java
@Path("/messages")
public class MessageResource {
    private URI uri = URI.create("http://localhost:8080/tp7_ipr/chat.html");

    // avoir la liste des messages
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessageList() {
        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        List<Message> messageList1 = messageList.getMessages();

        if (null == messageList1)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        Message[] messages = new Message[messageList1.size()];
        messages = messageList1.toArray(messages);

        return Response.ok().entity(messages).build();
    }

    // avoir les messages compris entre {id} et le dernier message du serveur
    @Path("/after/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessageListAfter(@PathParam("id") Long id) {
        if (null == id || id < 0)
            return Response.status(Response.Status.BAD_REQUEST).entity(new Message[0]).build();

        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        List<Message> messagesAfter = messageList.getMessagesAfter(id);

        if (null == messagesAfter)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        if (0 == messagesAfter.size())
            return Response.status(Response.Status.NO_CONTENT).entity(new Message[0]).build();

        Message[] messages = new Message[messagesAfter.size()];
        messages = messagesAfter.toArray(messages);

        return Response.ok().entity(messages).build();
    }

    // ajouter un message. Le message envoyé n'a pas d'id ni de date. C'est le serveur qui les initialise.
    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response addMessage(JAXBElement<Message> message) {
        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        Message mess = message.getValue();

        if (null == mess.getContent() || "".equals(mess.getContent().trim()))
            return Response.status(Response.Status.BAD_REQUEST).build();

        mess = messageList.createMessage(message.getValue());
        if (null == mess)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        return Response.status(Response.Status.CREATED).location(uri).build();
    }

    // avoir le message d'ID {id}
    @Path("/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessageID(@PathParam("id") Long id) {
        if (null == id || id < 0)
            return Response.status(Response.Status.BAD_REQUEST).entity(new Message()).build();

        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        Message message = messageList.getMessage(id);

        if (null == message)
            return Response.status(Response.Status.NO_CONTENT).entity(new Message()).build();

        return Response.ok().entity(message).build();
    }

    // effacer le message d'ID {id}
    @Path("/{id}")
    @DELETE
    public Response removeMessageID(@PathParam("id") Long id) {
        if (null == id || id < 0)
            return Response.status(Response.Status.BAD_REQUEST).entity("Wrong parameter value.").build();

        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        if (null == messageList.getMessage(id))
            return Response.status(Response.Status.NO_CONTENT).entity("Message " + id + " does not exist.").build();

        MessageList.getInstance().delMessage(id);
        return Response.ok("Message " + id + " successfully deleted.").build();
    }
}
```

*Résultats des codes de statut :*

![](pictures/chat_code_statut.png "pictures/chat_code_statut.png")


*Location après un ajout d'un message :*

![](pictures/header_location.png "pictures/header_location.png")

**4. Lancez votre service et tester qu'il fonctionne en consultant les pages :**

  - http://localhost:8080/pr.tp.services/api/messages

  ![](pictures/chat_messages.png "pictures/chat_messages.png")

  - http://localhost:8080/pr.tp.services/api/messages/2

  ![](pictures/chat_message_id.png "pictures/chat_message_id.png")

  - http://localhost:8080/pr.tp.services/api/messages/after/1

  ![](pictures/chat_messages_after.png "pictures/chat_messages_after.png")

**5. Donnez les commandes curl permettant :**

  - d'ajouter un nouveau message
  > curl -X POST -H 'Content-Type: application/json' -i http://localhost:8080/tp7_ipr/api/messages --data '{"content":"coucou"}'

  ![](pictures/curl_post.png "pictures/curl_post.png")

  - de consulter le nouveau message
  > curl -X GET -i http://localhost:8080/tp7_ipr/api/messages/3

  ![](pictures/curl_get.png "pictures/curl_get.png")

  - d'effacer le nouveau message
  > curl -X DELETE -i http://localhost:8080/tp7_ipr/api/messages/3

  ![](pictures/curl_delete.png "pictures/curl_delete.png")

## Test en Java

```java
 Client client = ClientBuilder.newClient();

        //Post a message - addMessage(JAXBElement<Message>)
        System.out.println("Post a message - addMessage(JAXBElement<Message>)");
        ChatMessage m = new ChatMessage();
        m.content = "test";
        Response response = client.target(URL).path("messages").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(m, MediaType.APPLICATION_XML));
        System.out.println("Response " + response.getStatus());
        System.out.println();

        //Get all the messages - getMessageList()
        System.out.println("Get all the messages - getMessageList()");
        ChatMessage[] messages = client.target(URL).path("messages").request(MediaType.APPLICATION_JSON_TYPE).get(ChatMessage[].class);
        for (ChatMessage chatMessage : messages) {
            System.out.println(chatMessage);
        }
        System.out.println();

        // GET @Path("/{id}")
        // Get the message number {id} - getMessageID(Long)
        System.out.println("Get the message number 2 - getMessageID(Long)");
        m = client.target(URL).path("messages").path("2").request(MediaType.APPLICATION_JSON_TYPE).get(ChatMessage.class);
        System.out.println(m);
        System.out.println();

        // DELETE @Path("/{id}")
        // Delete the message number 2 - removeMessageID(Long)
        System.out.println("Delete the message number 2 - removeMessageID(Long)");
        String result = client.target(URL).path("messages").path("2").request(MediaType.APPLICATION_JSON_TYPE).delete(String.class);
        System.out.println(result);
        System.out.println();

        // GET @Path("/after/{id}")
        //Get the messages between {id} and the last one - getMessageListAfter(Long)
        System.out.println("Get the messages between 1 and the last one - getMessageListAfter(Long)");
        messages = client.target(URL).path("messages").path("after").path("2").request(MediaType.APPLICATION_JSON_TYPE).get(ChatMessage[].class);
        for (ChatMessage chatMessage : messages) {
            System.out.println(chatMessage);
        }
        System.out.println();
```

*Résultat :*

  ![](pictures/test_chat_java.png "pictures/test_chat_java.png")

## Client du chat avec Javascript

*Bonus : vous pouvez ajouter une commande pour l'effacement des messages en vous inspirant du code existant.*

Le bouton d'effacement des messages possède la même logique que le bouton pour poster un message.

```javascript
    function removeMessage(messageid) {
        $.ajax({
            type: 'DELETE',
            url: rootURL + "/messages/" + messageid,
            success: function () {
                console.log("delete message " + messageid);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('delete error: ' + textStatus + " " + errorThrown);
            }
        });
        $('#' + messageid).remove();
    }
    
    function removeContent() {
        var ligne = $("#ligne2").val();
        $("#ligne2").val("");
        removeMessage(ligne);
    }

    $("#deleteButton").click(function () {
        removeContent();
    });
```

*Résultat :*

  ![](pictures/chat_interface.png "pictures/chat_interface.png")

## Ecriture du client en JAVA


Classe Afficheur qui implémente Runnable :

```java
    public void run() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            while (!stop) {
                Object jsonResponse = mapper.readValue(requestSender(), Object.class);
                System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonResponse));
                Thread.sleep(1000);
            }
        } catch (InterruptedException | IOException e) {
            Logger.getGlobal().severe("Le serveur n'est pas disponible");
        }
    }

    private String requestSender() throws IOException {
        String message;

        Socket socket = new Socket(host, port);

        PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), charset));
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));
        out.println("GET /tp7_ipr/api/messages HTTP/1.1");
        out.println("Host: " + host + ":" + port);
        out.println("Connection: close");
        out.println();
        out.flush();

        StringBuilder stringBuilder = new StringBuilder();
        // Retour du serveur
        while (null != (message = in.readLine())) {
            stringBuilder.append(message);
        }

        socket.close();
        return stringBuilder.toString().split("Connection: close")[1];
    }
```

Classe principale qui lit des messages sur le clavier et envoie les messages au serveur :

```java
        private final static Charset charset = StandardCharsets.UTF_8;
    private static String host = "localhost";
    private static int port = 8080;

    public static void main(String[] args) {
        Afficheur aff_run = new Afficheur(host, port);
        Thread aff_thread = new Thread(aff_run);
        aff_thread.start();

        try {
            String message;
            while (!("fin").equals((message = lireMessageAuClavier()))) {
                envoyerMessage(message);
            }
            aff_run.arret();
        } catch (IOException e) {
            aff_run.arret();
            Logger.getGlobal().severe("Le serveur n'est pas disponible");
        } finally {
            aff_run.arret();
        }
    }

    private static String lireMessageAuClavier() throws IOException {
        return new BufferedReader(new InputStreamReader(System.in, charset)).readLine();
    }

    private static void envoyerMessage(String message) throws IOException {
        String messageFormatted = message.replace("\"","\\\"");
        Socket socket = new Socket(host, port);

        PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), charset));
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));

        String body = "{\"content\":\"" + messageFormatted + "\"}\n";
        out.println("POST /tp7_ipr/api/messages HTTP/1.1");
        out.println("Host: " + host + ":" + port);
        out.println("Content-Type: application/json");
        out.println("Content-Length: " + body.length());
        out.println("Connection: close");
        out.println();
        out.println(body);
        out.flush();

        StringBuilder stringBuilder = new StringBuilder();
        // Retour du serveur
        while (null != (message = in.readLine())) {
            stringBuilder.append(message);
        }

        System.out.println(stringBuilder.toString());
        socket.close();
    }
```

*Résultat :*

  ![](pictures/test_client_java_affiche.png "pictures/test_client_java_affiche.png")
