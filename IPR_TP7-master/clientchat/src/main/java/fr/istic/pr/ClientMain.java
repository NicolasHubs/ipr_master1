package fr.istic.pr;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class ClientMain {
    private final static Charset charset = StandardCharsets.UTF_8;
    private static String host = "localhost";
    private static int port = 8080;

    public static void main(String[] args) {
        Afficheur aff_run = new Afficheur(host, port);
        Thread aff_thread = new Thread(aff_run);
        aff_thread.start();

        try {
            String message;
            while (!("fin").equals((message = lireMessageAuClavier()))) {
                envoyerMessage(message);
            }
            aff_run.arret();
        } catch (IOException e) {
            aff_run.arret();
            Logger.getGlobal().severe("Le serveur n'est pas disponible");
        } finally {
            aff_run.arret();
        }
    }

    private static String lireMessageAuClavier() throws IOException {
        return new BufferedReader(new InputStreamReader(System.in, charset)).readLine();
    }

    private static void envoyerMessage(String message) throws IOException {
        String messageFormatted = message.replace("\"","\\\"");
        Socket socket = new Socket(host, port);

        PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), charset));
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));

        String body = "{\"content\":\"" + messageFormatted + "\"}\n";
        out.println("POST /tp7_ipr/api/messages HTTP/1.1");
        out.println("Host: " + host + ":" + port);
        out.println("Content-Type: application/json");
        out.println("Content-Length: " + body.length());
        out.println("Connection: close");
        out.println();
        out.println(body);
        out.flush();

        StringBuilder stringBuilder = new StringBuilder();
        // Retour du serveur
        while (null != (message = in.readLine())) {
            stringBuilder.append(message);
        }

        System.out.println(stringBuilder.toString());
        socket.close();
    }
}
