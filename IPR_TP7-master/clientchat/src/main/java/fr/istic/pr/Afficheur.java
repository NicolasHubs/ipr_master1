package fr.istic.pr;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class Afficheur implements Runnable {
    private final static Charset charset = StandardCharsets.UTF_8;
    private String host;
    private int port;
    private boolean stop = false;

    public Afficheur(String host, int port) {
        this.host = host;
        this.port = port;
    }

    private String requestSender() throws IOException {
        String message;

        Socket socket = new Socket(host, port);

        PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), charset));
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));
        out.println("GET /tp7_ipr/api/messages HTTP/1.1");
        out.println("Host: " + host + ":" + port);
        out.println("Connection: close");
        out.println();
        out.flush();

        StringBuilder stringBuilder = new StringBuilder();
        // Retour du serveur
        while (null != (message = in.readLine())) {
            stringBuilder.append(message);
        }

        socket.close();
        return stringBuilder.toString().split("Connection: close")[1];
    }

    public void arret() {
        stop = true;
    }

    public void run() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            while (!stop) {
                Object jsonResponse = mapper.readValue(requestSender(), Object.class);
                System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonResponse));
                Thread.sleep(1000);
            }
        } catch (InterruptedException | IOException e) {
            Logger.getGlobal().severe("Le serveur n'est pas disponible");
        }
    }
}
