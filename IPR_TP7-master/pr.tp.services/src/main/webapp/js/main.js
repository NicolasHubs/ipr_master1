$(function () {

    var rootURL = "http://localhost:8080/tp7_ipr/api";

    var lastID = 0;

    function updateMessages() {
        $.ajax({
            type: 'GET',
            url: rootURL + "/messages/after/" + lastID,
            dataType: "json",
            success: function (data) {
                if (null != data) {
                    if ($.isArray(data)) {
                        console.log("append each message");
                        $.each(data, function (key, message) {
                            appendMessage(message);
                        });
                    } else {
                        console.log("append one message ");
                        appendMessage(data);
                    }

                }
                console.log("call update again in 1s");
                setTimeout(updateMessages, 1000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('updateMessages error: ' + textStatus + ' ' + errorThrown);
            }
        });
    }

    function appendMessage(message) {
        console.log(message);

        $('#chatContent')
        /*.append("<button type=\"button\" value=\"" + message.id + "\" id=\"deleteButton\">ok</button>")*/
            .append("<p id=\"" + message.id + "\" >" + message.id + ":" + message.date + ">" + message.content + "</p>");

        lastID = parseInt(message.id) + 1;
        console.log(lastID + " computed from " + message.id);

    }

    function sendMessage(messageContent) {
        $.ajax({
            type: 'POST',
            url: rootURL + "/messages",
            contentType: 'application/json',
            data: JSON.stringify({
                "content": messageContent
            }),
            dataType: "json",
            success: function (data) {
                console.log("create message " + data.id);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('sendMessage error: ' + textStatus + " " + errorThrown);
            }
        });
    }

    function removeMessage(messageid) {
        $.ajax({
            type: 'DELETE',
            url: rootURL + "/messages/" + messageid,
            success: function () {
                console.log("delete message " + messageid);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('delete error: ' + textStatus + " " + errorThrown);
            }
        });
        $('#' + messageid).remove();
    }

    function sendContent() {
        var ligne = $("#ligne").val();
        $("#ligne").val("");
        sendMessage(ligne);
    }

    function removeContent() {
        var ligne = $("#ligne2").val();
        $("#ligne2").val("");
        removeMessage(ligne);
    }

    $("#submitButton").click(function () {
        sendContent();
    });

    $("#deleteButton").click(function () {
        removeContent();
    });

    $(window).keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            sendContent();
            return false;
        }
    });

    updateMessages();

});