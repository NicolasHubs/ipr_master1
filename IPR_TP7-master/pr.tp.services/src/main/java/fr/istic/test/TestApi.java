package fr.istic.test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;

public class TestApi {

    //TODO : Adapter l'URL en fonction de votre resource :
    public static final String URL = "http://localhost:8080/tp7_ipr/api";

    @XmlRootElement(name = "message")
    public static class ChatMessage {
        public Long id;
        public String content;
        public String date;

        @Override
        public String toString() {
            return id + ":" + content + " at " + date;
        }
    }

    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();

        //Post a message - addMessage(JAXBElement<Message>)
        System.out.println("Post a message - addMessage(JAXBElement<Message>)");
        ChatMessage m = new ChatMessage();
        m.content = "test";
        Response response = client.target(URL).path("messages").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(m, MediaType.APPLICATION_XML));
        System.out.println("Response " + response.getStatus());
        System.out.println();

        //Get all the messages - getMessageList()
        System.out.println("Get all the messages - getMessageList()");
        ChatMessage[] messages = client.target(URL).path("messages").request(MediaType.APPLICATION_JSON_TYPE).get(ChatMessage[].class);
        for (ChatMessage chatMessage : messages) {
            System.out.println(chatMessage);
        }
        System.out.println();

        // GET @Path("/{id}")
        // Get the message number {id} - getMessageID(Long)
        System.out.println("Get the message number 2 - getMessageID(Long)");
        m = client.target(URL).path("messages").path("2").request(MediaType.APPLICATION_JSON_TYPE).get(ChatMessage.class);
        System.out.println(m);
        System.out.println();

        // DELETE @Path("/{id}")
        // Delete the message number 2 - removeMessageID(Long)
        System.out.println("Delete the message number 2 - removeMessageID(Long)");
        String result = client.target(URL).path("messages").path("2").request(MediaType.APPLICATION_JSON_TYPE).delete(String.class);
        System.out.println(result);
        System.out.println();

        // GET @Path("/after/{id}")
        //Get the messages between {id} and the last one - getMessageListAfter(Long)
        System.out.println("Get the messages between 1 and the last one - getMessageListAfter(Long)");
        messages = client.target(URL).path("messages").path("after").path("2").request(MediaType.APPLICATION_JSON_TYPE).get(ChatMessage[].class);
        for (ChatMessage chatMessage : messages) {
            System.out.println(chatMessage);
        }
        System.out.println();
    }
}