package fr.istic.rest;

import fr.istic.chat.message.Message;
import fr.istic.chat.message.MessageList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBElement;
import java.net.URI;
import java.util.List;

@Path("/messages")
public class MessageResource {
    private URI uri = URI.create("http://localhost:8080/tp7_ipr/chat.html");

    // avoir la liste des messages
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessageList() {
        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        List<Message> messageList1 = messageList.getMessages();

        if (null == messageList1)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        Message[] messages = new Message[messageList1.size()];
        messages = messageList1.toArray(messages);

        return Response.ok().entity(messages).build();
    }

    // avoir les messages compris entre {id} et le dernier message du serveur
    @Path("/after/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessageListAfter(@PathParam("id") Long id) {
        if (null == id || id < 0)
            return Response.status(Response.Status.BAD_REQUEST).entity(new Message[0]).build();

        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        List<Message> messagesAfter = messageList.getMessagesAfter(id);

        if (null == messagesAfter)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        if (0 == messagesAfter.size())
            return Response.status(Response.Status.NO_CONTENT).entity(new Message[0]).build();

        Message[] messages = new Message[messagesAfter.size()];
        messages = messagesAfter.toArray(messages);

        return Response.ok().entity(messages).build();
    }

    // ajouter un message. Le message envoyé n'a pas d'id ni de date. C'est le serveur qui les initialise.
    // curl -X POST -H "Content-Type: application/json" -d '{"content":"coucou"}' http://localhost:8080/tp7-ipr/api/messages | jq
    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response addMessage(JAXBElement<Message> message) {
        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        Message mess = message.getValue();

        if (null == mess.getContent() || "".equals(mess.getContent().trim()))
            return Response.status(Response.Status.BAD_REQUEST).build();

        mess = messageList.createMessage(message.getValue());
        if (null == mess)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        return Response.status(Response.Status.CREATED).location(uri).build();
    }

    // avoir le message d'ID {id}
    @Path("/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessageID(@PathParam("id") Long id) {
        if (null == id || id < 0)
            return Response.status(Response.Status.BAD_REQUEST).entity(new Message()).build();

        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        Message message = messageList.getMessage(id);

        if (null == message)
            return Response.status(Response.Status.NO_CONTENT).entity(new Message()).build();

        return Response.ok().entity(message).build();
    }

    // effacer le message d'ID {id}
    // curl -X DELETE http://localhost:8080/tp7-ipr/rest/messages/2 | jq
    @Path("/{id}")
    @DELETE
    public Response removeMessageID(@PathParam("id") Long id) {
        if (null == id || id < 0)
            return Response.status(Response.Status.BAD_REQUEST).entity("Wrong parameter value.").build();

        MessageList messageList = MessageList.getInstance();

        if (null == messageList)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

        if (null == messageList.getMessage(id))
            return Response.status(Response.Status.NO_CONTENT).entity("Message " + id + " does not exist.").build();

        MessageList.getInstance().delMessage(id);
        return Response.ok("Message " + id + " successfully deleted.").build();
    }
}