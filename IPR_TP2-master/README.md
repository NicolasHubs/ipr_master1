# TP 7 : Les services REST

## Partie 1 : protocole TFTP

### Exercice 1 : Observation

#### Ouvrez wireshark et observer sur localhost

 1. Y a-t-il une demande de connexion initiale ?
 > Non il n'y a pas de demande de connexion initiale.

 2. Capturez une demande de lecture du fichier /tmp/$nom.txt et une demande d'écriture de /tmp/$nom.txt ou vous remplacerez $nom par votre nom de famille.

*Demande de lecture :*

![](pictures/demande_lecture_tftp.png "pictures/demande_lecture_tftp.png")

*Demande d'écriture :*

![](pictures/demande_ecriture_tftp.png "pictures/demande_ecriture_tftp.png")

 3. Décrivez les champs de la requête en vous appuyant sur la RFC si nécessaire.

Dans les images ci-dessus, les deux premiers bytes de la zone en surbrillance correspondent à l'Opcode :  

 00 01     Read request (RRQ) -> GET

 00 02     Write request (WRQ) -> PUT

Les 13 bytes suivants correspondent au nom du fichier (ici /tmp/croq.txt) suivi d'un byte à zero, puis le mode (ici netascii de 8 bytes) et pour terminer un byte à zéro.

RRQ/WRQ packet :

            2 bytes     string    1 byte     string   1 byte
            ------------------------------------------------
           | Opcode |  Filename  |   0  |    Mode    |   0  |
            ------------------------------------------------

### Exercice 2 : Décodage des requêtes.

```java
    public static void main(String[] args) throws IOException {
        int portDuServeur = 6969;

        // Attends sur le port 6969
        DatagramSocket socketVersLeServeur = new DatagramSocket(portDuServeur);

        // Boucle
        while (!stop) {
            byte[] buffer = new byte[600];
            // Reception du packet
            DatagramPacket p = new DatagramPacket(buffer, buffer.length);
            socketVersLeServeur.receive(p);

            // Affichage du packet
            // Attention à ne pas afficher plus d'informations que nécessaire.
            affiche(buffer, p.getLength());

            // Décodage du packet
            decodeRequest(p);
        }
        socketVersLeServeur.close();
    }

    private static void affiche(byte[] bytes, int dataLength) {
        byte[] bytesCopy = Arrays.copyOfRange(bytes, 0, dataLength);
        for (int i = 0; i < bytesCopy.length; i++) {
            if (i % 16 == 0) {
                System.out.println("\n");
            }
            System.out.printf("%02x ", bytesCopy[i]);
        }
        System.out.println();
    }
```

*Résultat :*

![](pictures/tftp_decode_result_exo2.png "pictures/tftp_decode_result_exo2.png")

### Exercice 3 : Acquitter

```java
    public static void main(String[] args) throws IOException {
        int portDuServeur = 6969;

        // Attends sur le port 6969
        DatagramSocket socketVersLeServeur = new DatagramSocket(portDuServeur);

        int step = 0;
        // Boucle
        while (!stop) {

            byte[] buffer = new byte[600];
            // Reception du packet
            DatagramPacket p = new DatagramPacket(buffer, buffer.length);
            socketVersLeServeur.receive(p);

            // Affichage du packet
            // Attention à ne pas afficher plus d'informations que nécessaire.
            affiche(buffer, p.getLength());

            // Décodage du packet
            decodeRequest(p);

            // Envoyer acquittement
            sendAck(socketVersLeServeur, (step == 0) ? 0 : buffer[3], new InetSocketAddress(p.getAddress(), p.getPort()));
            step++;
        }
        socketVersLeServeur.close();
    }

    public static void sendAck(DatagramSocket server, short seqNumber, SocketAddress dstAddr) throws IOException {
        System.out.println("Send " + seqNumber + " to " + dstAddr);

        // Construire le paquet avec les bonnes informations
        ByteBuffer byteBuffer = ByteBuffer.allocate(4); //adapter la taille
        byteBuffer.putShort((short) 4);
        byteBuffer.putShort(seqNumber);
        byte[] bytesMessage = byteBuffer.array();
        DatagramPacket pSend = new DatagramPacket(bytesMessage, bytesMessage.length, dstAddr);

        // afficher le tableau de bytes envoyé
        affiche(bytesMessage, bytesMessage.length);

        // Envoyer le paquet à la bonnes addresses
        server.send(pSend);
    }

     public static void decodeRequest(DatagramPacket p) {

        // Opcode
        byte[] bytes = p.getData();
        String type;
        switch (bytes[1]) {
            case 1:
                type = "GET"; // Read request (RRQ)
                break;
            case 2:
                type = "PUT"; // Write request (WRQ)
                break;
            case 3:
                type = "DATA"; // Data (DATA)
                break;
            case 4:
                type = "ACK"; // Acknowledgment (ACK)
                break;
            default:
                type = "ERROR"; // Error (ERROR)
                break;
        }
        System.out.printf("\nType : %s", type);

        StringBuilder sb = new StringBuilder();
        int maxLen = bytes.length;
        int i;
        byte b;

        if (type.equals("GET") || type.equals("PUT")) {
            // Filename
            String filename;
            i = 2;
            while ((i < maxLen) && (b = bytes[i]) != 0) {
                sb.append((char) b);
                i++;
            }
            filename = sb.toString();

            // Mode
            String mode;
            sb = new StringBuilder();
            i++;
            while ((i < maxLen) && (b = bytes[i]) != 0) {
                sb.append((char) b);
                i++;
            }
            mode = sb.toString();
            System.out.printf(", fichier : %s, mode %s\n", filename, mode);
        } else if (type.equals("DATA")) {
            String data;
            i = 4;
            while ((i < maxLen) && (b = bytes[i]) != 0) {
                sb.append((char) b);
                i++;
            }
            data = sb.toString();
            System.out.printf(", data : %s\n", data);
        }
    }
```

*Résultat :*

![](pictures/tftp_decode_put_serveur.png "pictures/tftp_decode_put_serveur.png")

### Exercice 4 : Echange entre votre serveur sur wireshark

![](pictures/wireshark_serv.png "pictures/wireshark_serv.png")

## Partie 2 : Protocole de découverte multicast

Classe Discovery qui une fois instancié correspond à un service :

```java 
class Discovery {
    private MulticastSocket socket;
    private String ID;
    private String URL;
    private InetAddress groupIP;
    private Map<String, String> registeredServices;

    private boolean stop;

    Discovery(String ipAddress, int port, String ID, String URL) {
        this.ID = ID;
        this.URL = URL;
        stop = false;
        registeredServices = new HashMap<>();
        try {
            socket = new MulticastSocket(port);
            groupIP = InetAddress.getByName(ipAddress);
            socket.joinGroup(groupIP);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void sendWhois(String id) {
        // Demande à tous le monde qui est l'id
        envoyerMessage("WHOIS:" + id);
    }

    void sendLeaving() {
        // Envoie a tous le monde que tu quittes
        stop = true;
        envoyerMessage("LEAVING:" + ID);
    }

    void sendIAM() {
        // Envoie qui tu es à tous le monde (ton id et ton url)
        envoyerMessage("IAM:" + ID + ":" + URL);
    }

    void handleListen() {
        String[] message;
        sendIAM();
        while (!stop) {
            message = recevoirMessage().split(":", 3);

            if (stop)
                break;

            // On parse le message
            String cmd = "", id = "", url = "", query = "";
            if (message.length > 0) {
                cmd = message[0].toUpperCase();
                query += cmd;
                if (message.length > 1) {
                    id = message[1];
                    query += ":" + id;
                    if (message.length > 2) {
                        url = message[2];
                        query += ":" + url;
                    }
                }
            }

            // Ecoute et affiche les événements IAM, LEAVING, WHOIS
            // On évite d'afficher nos propres IAM et LEAVING en filtrant un minimum
            if (!id.equals(ID) || !url.equals(URL)) {
                System.out.println("here " + ID + ", recieved : " + query);
                switch (cmd) {
                    case "IAM":
                        registeredServices.put(id, url);
                        break;
                    case "LEAVING":
                        registeredServices.remove(id);
                        break;
                    case "WHOIS":
                        if (id.equals(ID))
                            sendIAM();
                        break;
                    default:
                        System.out.println("Erreur de commande");
                        break;
                }
            }
        }
        socket.close();
        registeredServices.clear();
    }

    private String recevoirMessage() {
        byte[] messageContent = new byte[1024];
        DatagramPacket packet = new DatagramPacket(messageContent, messageContent.length);
        try {
            socket.receive(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(Arrays.copyOfRange(messageContent, 0, packet.getLength()));
    }

    private void envoyerMessage(String message) {
        if (message.length() <= 1024) { // La taille maximale des messages est de 1024 caractères
            byte[] messageContent = Arrays.copyOfRange(message.getBytes(), 0, message.length());
            DatagramPacket packet = new DatagramPacket(messageContent, messageContent.length, groupIP, socket.getLocalPort());
            try {
                socket.send(packet);    // Envoyer le message
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
            System.out.println("Votre message a dépassé la taille maximale autorisée. (1024 caractères)");
    }
}
```

Classe de test et scénario dans laquelle deux services vont s'enregistrer et communiquer (à noter qu'à chaque démarrage d'un service, ce dernier envoie aux autres IAM:sonID:sonURL) :

Scénario de test résumé (l'ordre est juste là à titre indicatif, il n'est pas forcément correct car ce sont des threads) : 
- Le service 1 se connecte et envoie IAM:serv1_id:serv1_url
- Le service 2 se connecte et envoie IAM:serv2_id:serv2_url
- Le service 1 envoie WHOIS:serv2_id
- Le service 2 envoie IAM:serv2_id:serv2_url (en reponse au message précédent)
- Le service 2 envoie WHOIS:serv1_id
- Le service 1 envoie IAM:serv1_id:serv1_url
- Le service 1 envoie LEAVING:serv1_id
- Le service 2 envoie LEAVING:serv2_id

```java 
    public static void main(String[] args) throws InterruptedException {
        Discovery service1 = new Discovery(
                "225.0.4.1",
                9999,
                "067e6162",
                "http://192.168.0.48/lampe1/switch");
        new Thread(service1::handleListen).start();

        Discovery service2 = new Discovery(
                "225.0.4.1",
                9999,
                "034e6ef2",
                "http://192.168.0.13:8888/frigo/controller");
        new Thread(service2::handleListen).start();

        service1.sendWhois("034e6ef2");
        service2.sendWhois("067e6162");

        Thread.sleep(1000);
        service1.sendLeaving();

        Thread.sleep(500);
        service2.sendLeaving();
    }
```


*Les résultats affichés correspondent au messages reçus.
Résultat :*

![](pictures/frigo_switch.png "pictures/frigo_switch.png")


