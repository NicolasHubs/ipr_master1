package pr.tp2.udp.discovery;

public class TestDiscovery {

    public static void main(String[] args) throws InterruptedException {
        Discovery service1 = new Discovery(
                "225.0.4.1",
                9999,
                "067e6162",
                "http://192.168.0.48/lampe1/switch");
        new Thread(service1::handleListen).start();

        Discovery service2 = new Discovery(
                "225.0.4.1",
                9999,
                "034e6ef2",
                "http://192.168.0.13:8888/frigo/controller");
        new Thread(service2::handleListen).start();

        service1.sendWhois("034e6ef2");
        service2.sendWhois("067e6162");

        Thread.sleep(1000);
        service1.sendLeaving();

        Thread.sleep(500);
        service2.sendLeaving();
    }
}
