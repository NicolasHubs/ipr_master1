package pr.tp2.udp.discovery;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class Discovery {
    private MulticastSocket socket;
    private String ID;
    private String URL;
    private InetAddress groupIP;
    private Map<String, String> registeredServices;

    private boolean stop;

    Discovery(String ipAddress, int port, String ID, String URL) {
        this.ID = ID;
        this.URL = URL;
        stop = false;
        registeredServices = new HashMap<>();
        try {
            socket = new MulticastSocket(port);
            groupIP = InetAddress.getByName(ipAddress);
            socket.joinGroup(groupIP);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void sendWhois(String id) {
        // Demande à tous le monde qui est l'id
        envoyerMessage("WHOIS:" + id);
    }

    void sendLeaving() {
        // Envoie a tous le monde que tu quittes
        stop = true;
        envoyerMessage("LEAVING:" + ID);
    }

    void sendIAM() {
        // Envoie qui tu es à tous le monde (ton id et ton url)
        envoyerMessage("IAM:" + ID + ":" + URL);
    }

    void handleListen() {
        String[] message;
        sendIAM();
        while (!stop) {
            message = recevoirMessage().split(":", 3);

            if (stop)
                break;

            String cmd = "", id = "", url = "", query = "";
            if (message.length > 0) {
                cmd = message[0].toUpperCase();
                query += cmd;
                if (message.length > 1) {
                    id = message[1];
                    query += ":" + id;
                    if (message.length > 2) {
                        url = message[2];
                        query += ":" + url;
                    }
                }
            }

            // Ecoute et affiche les événements IAM, LEAVING, WHOIS
            // On évite d'afficher nos propres IAM et LEAVING en filtrant un minimum
            if (!id.equals(ID) || !url.equals(URL)) {
                System.out.println("here " + ID + ", recieved : " + query);

                switch (cmd) {
                    case "IAM":
                        registeredServices.put(id, url);
                        break;
                    case "LEAVING":
                        registeredServices.remove(id);
                        break;
                    case "WHOIS":
                        if (id.equals(ID))
                            sendIAM();
                        break;
                    default:
                        System.out.println("Erreur de commande");
                        break;
                }
            }
        }
        socket.close();
        registeredServices.clear();
    }

    private String recevoirMessage() {
        byte[] messageContent = new byte[1024];
        DatagramPacket packet = new DatagramPacket(messageContent, messageContent.length);
        try {
            socket.receive(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(Arrays.copyOfRange(messageContent, 0, packet.getLength()));
    }

    private void envoyerMessage(String message) {
        if (message.length() <= 1024) { // La taille maximale des messages est de 1024 caractères
            byte[] messageContent = Arrays.copyOfRange(message.getBytes(), 0, message.length());
            DatagramPacket packet = new DatagramPacket(messageContent, messageContent.length, groupIP, socket.getLocalPort());
            try {
                socket.send(packet);    // Envoyer le message
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
            System.out.println("Votre message a dépassé la taille maximale autorisée. (1024 caractères)");
    }
}
