package pr.tp2.udp.tftp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;

public class TftpDecode {
    private static boolean stop = false;

    public static void main(String[] args) throws IOException {
        int portDuServeur = 6969;

        // Attends sur le port 6969
        DatagramSocket socketVersLeServeur = new DatagramSocket(portDuServeur);

        // Boucle
        while (!stop) {
            byte[] buffer = new byte[600];
            // Reception du packet
            DatagramPacket p = new DatagramPacket(buffer, buffer.length);
            socketVersLeServeur.receive(p);

            // Affichage du packet
            // Attention à ne pas afficher plus d'informations que nécessaire.
            affiche(buffer, p.getLength());

            // Décodage du packet
            decodeRequest(p);
        }
        socketVersLeServeur.close();
    }

    private static void affiche(byte[] bytes, int dataLength) {
        byte[] bytesCopy = Arrays.copyOfRange(bytes, 0, dataLength);
        for (int i = 0; i < bytesCopy.length; i++) {
            if (i % 16 == 0) {
                System.out.println("\n");
            }
            System.out.printf("%02x ", bytesCopy[i]);
        }
        System.out.println();
    }

    private static void decodeRequest(DatagramPacket p) {
        byte[] bytes = p.getData();
        String type = (bytes[1] == 1 ? "GET" : "PUT");
        String[] fileNameMode = getFileNameAndMode(bytes);

        System.out.printf("\nType : %s, fichier : %s, mode %s", type, fileNameMode[0], fileNameMode[1]);
    }

    private static String[] getFileNameAndMode(byte[] inData) {
        int maxLen = inData.length;
        StringBuilder sb = new StringBuilder();
        String[] stringTab = new String[2];
        int i = 2;
        byte b;

        while ((i < maxLen) && (b = inData[i]) != 0) {
            final int v = ((int) b) & 0xff;
            sb.append((char) v);
            i++;
        }
        stringTab[0] = sb.toString();

        sb = new StringBuilder();
        i++;
        while ((i < maxLen) && (b = inData[i]) != 0) {
            final int v = ((int) b) & 0xff;
            sb.append((char) v);
            i++;
        }
        stringTab[1] = sb.toString();
        return stringTab;
    }
}
