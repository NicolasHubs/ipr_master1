package pr.tp2.udp.tftp;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Date;

public class TftpPutServeur {
    private static boolean stop = false;

    public static void main(String[] args) throws IOException {
        int portDuServeur = 6969;

        // Attends sur le port 6969
        DatagramSocket socketVersLeServeur = new DatagramSocket(portDuServeur);

        int step = 0;
        // Boucle
        while (!stop) {

            byte[] buffer = new byte[600];
            // Reception du packet
            DatagramPacket p = new DatagramPacket(buffer, buffer.length);
            socketVersLeServeur.receive(p);

            // Affichage du packet
            // Attention à ne pas afficher plus d'informations que nécessaire.
            affiche(buffer, p.getLength());

            // Décodage du packet
            decodeRequest(p);

            // Envoyer acquittement
            sendAck(socketVersLeServeur, (step == 0) ? 0 : buffer[3], new InetSocketAddress(p.getAddress(), p.getPort()));
            step++;
        }
        socketVersLeServeur.close();
    }

    public static void sendAck(DatagramSocket server, short seqNumber, SocketAddress dstAddr) throws IOException {
        System.out.println("Send " + seqNumber + " to " + dstAddr);

        // Construire le paquet avec les bonnes informations
        ByteBuffer byteBuffer = ByteBuffer.allocate(4); //adapter la taille
        byteBuffer.putShort((short) 4);
        byteBuffer.putShort(seqNumber);
        byte[] bytesMessage = byteBuffer.array();
        DatagramPacket pSend = new DatagramPacket(bytesMessage, bytesMessage.length, dstAddr);

        // afficher le tableau de bytes envoyé
        affiche(bytesMessage, bytesMessage.length);

        // Envoyer le paquet à la bonnes addresses
        server.send(pSend);
    }

    public static void affiche(byte[] bytes, int dataLength) {
        byte[] bytesCopy = Arrays.copyOfRange(bytes, 0, dataLength);
        System.out.print("Contenu du tableau :");
        for (int i = 0; i < bytesCopy.length; i++) {
            if (i % 16 == 0) {
                System.out.println("\n");
            }
            System.out.printf("%02x ", bytesCopy[i]);
        }
        System.out.println();
    }

    public static void decodeRequest(DatagramPacket p) {

        // Opcode
        byte[] bytes = p.getData();
        String type;
        switch (bytes[1]) {
            case 1:
                type = "GET"; // Read request (RRQ)
                break;
            case 2:
                type = "PUT"; // Write request (WRQ)
                break;
            case 3:
                type = "DATA"; // Data (DATA)
                break;
            case 4:
                type = "ACK"; // Acknowledgment (ACK)
                break;
            default:
                type = "ERROR"; // Error (ERROR)
                break;
        }
        System.out.printf("\nType : %s", type);

        StringBuilder sb = new StringBuilder();
        int maxLen = bytes.length;
        int i;
        byte b;

        if (type.equals("GET") || type.equals("PUT")) {
            // Filename
            String filename;
            i = 2;
            while ((i < maxLen) && (b = bytes[i]) != 0) {
                sb.append((char) b);
                i++;
            }
            filename = sb.toString();

            // Mode
            String mode;
            sb = new StringBuilder();
            i++;
            while ((i < maxLen) && (b = bytes[i]) != 0) {
                sb.append((char) b);
                i++;
            }
            mode = sb.toString();
            System.out.printf(", fichier : %s, mode %s\n", filename, mode);
        } else if (type.equals("DATA")) {
            String data;
            i = 4;
            while ((i < maxLen) && (b = bytes[i]) != 0) {
                sb.append((char) b);
                i++;
            }
            data = sb.toString();
            System.out.printf(", data : %s\n", data);
        }
    }
}
