package fr.istic.pr.echo;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EchoServer {
    private static final Logger LOGGER = Logger.getLogger(ClientHandlerBytes.class.getName());
    private static boolean useBytesHandler = false;

    public static void main(String[] args) {

        /* Pour chaque client :
            1. accepter la connexion.
            2. créer un ClientHandler
            3. appeler la méthode handleBytes() sur le handler
        */

        //Attente sur le port 8080
        int portEcoute = 8080;

        try (ServerSocket socketServeur = new ServerSocket(portEcoute)) {
            LOGGER.info("localhost ou " + InetAddress.getLocalHost().getHostAddress());

            // Dans une boucle, pour chaque socket clientes, traiter le client
            while (true) {
                // Attente des connexions sur le port 8080
                Socket socketVersUnClient = socketServeur.accept();
                if (useBytesHandler)
                    new ClientHandlerBytes(socketVersUnClient).handle();
                else
                    new ClientHandlerChar(socketVersUnClient).handle();
            }
            // Traitement des exceptions
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Le port " + portEcoute + " est déjà utilisé.");
        }
    }
}