package fr.istic.pr.echo;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientHandlerChar implements ClientHandler {
    private Socket socket;
    private static final Logger LOGGER = Logger.getLogger(ClientHandlerBytes.class.getName());

    public ClientHandlerChar(Socket socket) {
        this.socket = socket;
    }

    public void handle() {
        //Lecture du message dans un buffer de bytes
        //Envoie du buffer de bytes.

        String requete;
        try {
            // Créer printer et reader
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
            PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));

            // Tant qu'il y a un message à lire
            while (null != (requete = in.readLine())) {
                System.out.println("Requete : " + requete);

                // Envoyer message au client via envoyerMessage
                envoyerMessage(out, requete);
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "L'utilisateur s'est déconnecté brutalement.");
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "La socket n'existe plus.");
            }
        }
    }

    public static void envoyerMessage(PrintWriter printer, String message) throws IOException {
        // Envoyer le message vers le client
        printer.println(message);
        printer.flush();
    }
}
