package fr.istic.pr.echo;

public interface ClientHandler {
    /** La méthode handle traite le client **/
    public void handle();
}