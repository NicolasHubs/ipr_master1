package fr.istic.pr.echo;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientHandlerBytes implements ClientHandler {

    private static final Logger LOGGER = Logger.getLogger(ClientHandlerBytes.class.getName());
    private Socket socket;
    private boolean isDisconnected = false;

    public ClientHandlerBytes(Socket socket) {
        this.socket = socket;
    }

    public void handle() {
        //Lecture du message dans un buffer de bytes
        //Envoi du buffer de bytes.

        int bufferSize = 8;

        try {
            ByteBuffer byteBuffer = ByteBuffer.allocate(bufferSize);
            BufferedInputStream inFromClient = new BufferedInputStream(socket.getInputStream());
            DataOutputStream outFromServer = new DataOutputStream(socket.getOutputStream());
            byte[] message;

            while (!isDisconnected) {
                while (byteBuffer.hasRemaining()) {
                    int b = inFromClient.read();
                    if (b == -1) {
                        isDisconnected = true;
                        break;
                    }

                    byteBuffer.put((byte) b);
                    if (inFromClient.available() == 0)
                        break;
                }

                if (isDisconnected)
                    break;
                message = new byte[byteBuffer.position()];
                for (int i = 0; i < message.length; i++) {
                    message[i] = byteBuffer.get(i);
                }

                LOGGER.info("Message reçu : " + new String(message));

                outFromServer.write(message);
                byteBuffer.clear();
            }
            LOGGER.info("La connexion s'est terminée correctement.");

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "L'utilisateur s'est déconnecté brutalement.");
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "La socket n'existe plus.");
            }
        }
    }
}