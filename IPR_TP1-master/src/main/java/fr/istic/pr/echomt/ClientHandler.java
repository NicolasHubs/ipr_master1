package fr.istic.pr.echomt;

public interface ClientHandler {
    /** La méthode handle traite le client **/
    public void handle();
}