package fr.istic.pr.ping;

import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class HttpPing {
    private final static Charset charset = StandardCharsets.UTF_8;

    public static class PingInfo {
        //Le temps de réponse :
        long time;
        //Le code de réponse :
        int code;

        @Override
        public String toString() {
            return String.format("[code %d -- %d ms]", code, time);
        }
    }

    public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
        System.out.println(ping("www.google.fr", 443));
    }

    public static PingInfo ping(String host, int port) throws UnknownHostException, IOException {
        PingInfo info = new PingInfo();
        String message;

        long time = System.currentTimeMillis();

        SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        Socket socket = factory.createSocket(host, port);
        // Socket socket = new Socket(host, port);

        // UTILISER PrintWriter et BufferedReader
        PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), charset));
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));
        out.println("GET / HTTP/1.1");
        out.println("Host: " + host);

        /// Envoyer l'entête
        // Penser à préciser l'Host dans l'en-tête

        // L'en-tête doit également contenir :
        out.println("Connection: close"); //demande au site de fermer la connexion après la réponse.

        // et se terminer par l'envoi d'une ligne vide sans espace
        out.println();
        out.flush();

        // LECTURE DE LA REPONSE
        info.code = Integer.valueOf(in.readLine().split(" ")[1]);


        //AFFICHAGE DE LA PAGE
        while (null != (message = in.readLine())) {
            System.out.println(message);
        }

        info.time = System.currentTimeMillis() - time;
        return info;
    }
}