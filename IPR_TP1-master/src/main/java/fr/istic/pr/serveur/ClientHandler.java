package fr.istic.pr.serveur;

public interface ClientHandler {
    /** La méthode handle traite le client **/
    public void handle();
}