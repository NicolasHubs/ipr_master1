package fr.istic.pr.serveur;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServeurHTTP {
    private static final Logger LOGGER = Logger.getLogger(ServeurHTTP.class.getName());

    public static void main(String[] args) {

        /* Pour chaque client :
            1. accepter la connexion.
            2. créer un ClientHandler
            3. appeler la méthode handleBytes() sur le handler
        */

        //Attente sur le port 8080
        int portEcoute = 8080;

        try (ServerSocket socketServeur = new ServerSocket(portEcoute)) {
            LOGGER.info("localhost ou " + InetAddress.getLocalHost().getHostAddress());
            Executor service = Executors.newFixedThreadPool(4);

            // Dans une boucle, pour chaque socket clientes, lancer un thread qui traite un client
            while (true) {
                // Attente des connexions sur le port 8080
                Socket socketVersUnClient = socketServeur.accept();
                service.execute(new HTTPHandler(socketVersUnClient));
            }
            // Traitement des exceptions
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Le port " + portEcoute + " est déjà utilisé.");
        }
    }
}