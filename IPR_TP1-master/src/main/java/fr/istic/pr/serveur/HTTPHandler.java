package fr.istic.pr.serveur;

/*imports*/

import com.sun.net.httpserver.HttpExchange;
import fr.istic.pr.echo.ClientHandlerBytes;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HTTPHandler implements ClientHandler, Runnable {
    private static final Logger LOGGER = Logger.getLogger(ClientHandlerBytes.class.getName());
    private Socket socket;
    private boolean isConnectionOver = false;

    public HTTPHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void handle() {
        try {
            // Crée les print writer et buffered reader
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
            PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
            ExchangeData exchange;
            String header;
            String headerParts[];
            while (!isConnectionOver) {
                exchange = new ExchangeData();
                header = in.readLine(); // Pour la gestion du type de la requete
                if (null != header) {
                    headerParts = header.split(" ");
                    if (headerParts.length > 1) {
                        exchange.setRequestMethod(header.split(" ")[0]);
                        exchange.setURI(header.split(" ")[1]);
                        exchange.setHTTP_Version(header.split(" ")[2]);

                        while (null != (header = in.readLine())) {
                            // lit l'entête de la requête
                            if (header.equals(""))
                                break;

                            headerParts = header.split(":", 2);
                            exchange.addRequestHeader(headerParts[0], headerParts[1]);
                        }

                        // Appelle doGet si la méthode est une méthode GET.
                        if (exchange.getRequestMethod().equals("GET")) {
                            doGet("./www", exchange);
                        } else {
                            set405Response(exchange);
                        }
                        sendResponse(exchange, out);
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "L'utilisateur s'est déconnecté brutalement.");
        }
    }

    public void sendResponse(ExchangeData exchange, PrintWriter out) {
        System.out.println();
        System.out.println(exchange.getHTTP_Version() + " " + exchange.getResponseCode() + " " + exchange.getCodeMessage());
        out.println(exchange.getHTTP_Version() + " " + exchange.getResponseCode() + " " + exchange.getCodeMessage());

        for (Map.Entry mapentry : exchange.getResponseHeaders().entrySet()) {
            out.println(mapentry.getKey() + ": " + mapentry.getValue());
            System.out.println(mapentry.getKey() + ": " + mapentry.getValue());
        }

        out.println();

        if (200 == exchange.getResponseCode()) {
            out.println(exchange.getFileContent());
        } else {
            System.out.println(exchange.getResponseCode() + " " + exchange.getCodeMessage());
            out.println(exchange.getResponseCode() + " " + exchange.getCodeMessage());
        }
        out.flush();
    }

    public void doGet(String websiteRootPath, ExchangeData exchange) {
        // Vérifie que le fichier existe
        System.out.println(websiteRootPath + exchange.getURI());
        File f = new File(websiteRootPath + exchange.getURI());

        // si le fichier existe :
        if (f.exists() && !f.isDirectory()) {
            //Ecrit l'en-tête de réponse (Code, Content-type, Connexion, ...)
            set200Response(exchange);

            //Ecrit le contenu du fichier si il existe
            exchange.setFileContent(getFileContent(websiteRootPath + exchange.getURI()));
        } else { // sinon
            // appelle la méthode send404.
            set404Response(exchange);
        }
        exchange.addResponseHeader("Content-Type", "text/html; charset=UTF-8");
        exchange.addResponseHeader("Connection", "close");
    }

    public String getFileContent(String pagepath) {
        StringBuilder content = new StringBuilder();
        File f = new File(pagepath);
        try {
            if (f.exists() && !f.isDirectory()) {
                BufferedReader fin = new BufferedReader(new FileReader(f));
                String line = "";
                while ((line = fin.readLine()) != null) {
                    content.append(line + "\n");
                }
            }
        } catch (IOException e) {
            // TODO : LOGGER file not found
        }
        return content.toString();
    }

    public void set200Response(ExchangeData exchange) {
        //Envoie une réponse 200 si l'echange s'est déroulé correctement
        exchange.setResponseCode(200);
        exchange.setCodeMessage("OK");
    }

    public void set404Response(ExchangeData exchange) {
        //Envoie une réponse 404 si le fichier n'existe pas.
        exchange.setResponseCode(404);
        exchange.setCodeMessage("Error : Le fichier n'existe pas.");
    }

    public void set405Response(ExchangeData exchange) {
        //Envoie une réponse avec le code 405 : Method Not Allowed
        exchange.setResponseCode(405);
        exchange.setCodeMessage("Error : Method Not Allowed");
    }

    @Override
    public void run() {
        handle();
    }
}