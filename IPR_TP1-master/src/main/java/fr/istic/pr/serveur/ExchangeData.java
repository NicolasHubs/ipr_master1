package fr.istic.pr.serveur;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ExchangeData {
    private Map<String, String> requestHeaders;
    private Map<String, String> responseHeaders;
    private String URI;
    private String requestMethod;
    private String HTTP_Version;
    private int responseCode;
    private String codeMessage;
    private String fileContent;

    public ExchangeData() {
        requestHeaders = new HashMap<>();
        responseHeaders = new HashMap<>();
        URI = "";
        requestMethod = "";
        HTTP_Version = "";
        responseCode = 200;
        codeMessage = "";
        fileContent = "";
    }

    public void addRequestHeader(String key, String value) {
        if (!"".equals(key) && !"".equals(value))
            requestHeaders.put(key, value);
    }

    public void removeRequestHeader(String key) {
        if (requestHeaders.containsKey(key))
            requestHeaders.remove(key);
    }

    public Map<String, String> getRequestHeaders() {
        return Collections.unmodifiableMap(requestHeaders);
    }

    public void addResponseHeader(String key, String value) {
        if (!"".equals(key) && !"".equals(value))
            responseHeaders.put(key, value);
    }

    public void removeResponseHeader(String key) {
        if (responseHeaders.containsKey(key))
            responseHeaders.remove(key);
    }

    public Map<String, String> getResponseHeaders() {
        return Collections.unmodifiableMap(responseHeaders);
    }

    public void setURI(String URI){
        this.URI = URI;
    }

    public String getURI(){
        return URI;
    }

    public void setRequestMethod(String requestMethod){
        this.requestMethod = requestMethod;
    }

    public String getRequestMethod(){
        return requestMethod;
    }

    public void setHTTP_Version(String HTTP_Version){
        this.HTTP_Version = HTTP_Version;
    }

    public String getHTTP_Version(){
        return HTTP_Version;
    }

    public void setResponseCode(int responseCode){
        this.responseCode = responseCode;
    }

    public int getResponseCode(){
        return responseCode;
    }

    public void setCodeMessage(String codeMessage){
        this.codeMessage = codeMessage;
    }

    public String getCodeMessage(){
        return codeMessage;
    }

    public void setFileContent(String fileContent){
        this.fileContent = fileContent;
    }

    public String getFileContent(){
        return fileContent;
    }
}
